import os
import tempfile
from App import *
import pytest
import unittest
import requests


class testApp(unittest.TestCase):

    def testWebpage(self):
        with app.test_client() as c:
            response = c.get('http://127.0.0.1:5000/login')
            self.assertEquals(response.status_code, 200)
    
    def testLogin(self):
        jsonToSend = {'user' : 'admin', 'pwd' : 'admin'}
        with app.test_client() as c:
            self.assertEquals(requests.post('http://127.0.0.1:5000/livedata', jsonToSend).status_code, 200)


if __name__=='__main__':
    unittest.main()