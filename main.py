# import the Flask class from the flask module
from flask import Flask, render_template, redirect, url_for, request, Response
import requests
import json

# create the application object
app = Flask(__name__)

# use decorators to link the function to a url
@app.route('/')
def home():
    return render_template('login.html')

@app.route('/welcome')
def welcome():
    while(1):
        response = requests.get(url_for('StreamData'))
        print(response.json())
        return render_template('login.html') # render a template

# Route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        if request.form['user'] != 'admin' or request.form['pwd'] != 'admin':
            error = 'Invalid Credentials. Please try again.'
        else:
            return redirect(url_for('home'))
    return render_template('login.html', error=error)

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)