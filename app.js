import React, { useState } from 'react';
import { useObservable } from 'rxjs-hooks';
import { Observable } from 'rxjs';
import { map, withLatestFrom } from 'rxjs/operators';

import { endpointUrls } from './config';

const stringObservable = Observable.create(observer => {
  const source = new EventSource(endpointUrls.dockerurlwindowsvb);
  source.addEventListener('message', (messageEvent) => {
    console.log(messageEvent);
    observer.next(messageEvent.data);
  }, false);
});

function App() {
  const [stringArray, setStringArray] = useState([]);

  useObservable(
    state =>
      stringObservable.pipe(
        withLatestFrom(state),
        map(([state]) => {
          let updatedStringArray = stringArray;
          updatedStringArray.unshift(state);
          if (updatedStringArray.length >= 50) {
            updatedStringArray.pop();
          }
          setStringArray(updatedStringArray);
          return state;
        })
      )
  );

  return (
    <div className="container">
      <Header />
      <div className="container">
        {!onlineStatus && !loading ? (
          <h3>The data server may be offline, changes will not be saved</h3>
        ) : null}
        {/* Components modified to allow editing of Todos */}
        <AllTodos allTodos={todos} loading={loading} selectTodo={selectTodo} />
        <AddTodo
          submitTodo={submitTodo}
          todos={todos}
          todoToUpdate={todoToUpdate}
        />
      </div>
      <Footer />
    </div>
  );
}

export default App;