from RandomDealData import *
import json
from flask import Flask, Response

app = Flask(__name__)

@app.route('/getdatastream', methods = ['GET'])
def StreamData():
    rdd = RandomDealData()
    rddil = rdd.createInstrumentList()
    rddf = rdd.createRandomData(rddil)
    rddfJson = json.loads(rddf)
    resposneJ = Response(rddfJson, status=200, mimetype='application/json')
    return(rddfJson)


# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)